<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'food' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']8uRoeC8cUCTr-mfW=+hycVBqL>@Qb_l(|.|B7-=gj tL~V=V!4:7ZjRdXo* XmP' );
define( 'SECURE_AUTH_KEY',  'o<i[0+&LdcBi+i)!rnS-Sa}]Y:Y/Wt<U0uo&qoYJRSu8$ob_Wzh9f02wT&$sn.2$' );
define( 'LOGGED_IN_KEY',    ' @c)D0(ihl8Khojbmxl 6@RX(%/d1nwjFT2_V~vJF6c&%jE-lksQg`_CNk;]m1>U' );
define( 'NONCE_KEY',        'tK{QUBe..lv&|3-]_acol<c5#;Ph{xG}n.0e,;xX`]rXD)IRFR>XVS0$_IG+X+?W' );
define( 'AUTH_SALT',        '4ujFqwdS=Hg3|k5>t!:;h}gdQP{Oq-#ik}[M)T T0d$ DQje;pt=IVEc?beIvO6O' );
define( 'SECURE_AUTH_SALT', 'IkG}d8KiQj m(N/YUs-}%B_ewEN-+ 8m<%icrj#0) dS~?bk#A0+&&1Aa}@qp)Vx' );
define( 'LOGGED_IN_SALT',   'z0Nsyq7A:u/Mc>Z4t6r*5v%1oBX!G*FAB_+Y|4) p~?-O_[ysM?;2Mrfok{7J(`Q' );
define( 'NONCE_SALT',       '*5H?{>8v|=,Iw-~oNm8V$j14@NnB,.|#^T3dQmF*+W_y,3Ea?lcDq`lCcxUL+Ia&' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
