<?php 
$method_ship = exwoofood_get_option('exwoofood_enable_method','exwoofood_shpping_options');
if($method_ship==''){ return;}
$class = '';
$user_address = WC()->session->get( '_user_deli_adress' );
$user_odmethod = WC()->session->get( '_user_order_method' );
//if($user_address == ''){
    //$class = 'ex-popup-active';
//}
global $locations;
$loc_sl = exwoofood_loc_field_html($locations);
?>
<div class="exwf-order-method">
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.exwf-opcls-info.exwf-odtype').addClass('ex-popup-active');
            jQuery('body').on('click', '.exwf-button', function (event) {
                var $method = 'delivery';
                if(jQuery(this).closest(".exwf-method-ct").find('.exwf-order-take.at-method').length){
                    $method = 'takeaway';
                }
                jQuery('.exwf-add-error').fadeOut();
                var $addr ='';
                var $cnt = 1;
                if($method != 'takeaway'){
                    $addr = jQuery(this).closest(".exwf-method-ct").find('#exwf-user-address').val();
                    if($addr==''){ 
                        jQuery('.exwf-del-address .exwf-add-error').fadeIn();
                        $cnt = 0;
                    }
                }
                var $loc = jQuery(this).closest(".exwf-method-ct").find('.exwf-del-log select').val();
                if(jQuery('.exwf-del-log select.ex-logreq').length && ($loc==null || $loc=='' )){ 
                    jQuery('.exwf-del-log .exwf-add-error').fadeIn();
                    $cnt = 0;
                }
                if($cnt == 0){ return;}
                jQuery('.exwf-method-ct').addClass('ex-loading');
                var ajax_url        = jQuery('.ex-fdlist input[name=ajax_url]').val();
                var param = {
                    action: 'exwf_check_distance',
                    address: $addr,
                    log: $loc,
                    method: $method,
                };
                jQuery.ajax({
                    type: "post",
                    url: ajax_url,
                    dataType: 'json',
                    data: (param),
                    success: function(data){
                        if(data != '0'){
                            jQuery('.exwf-method-ct').removeClass('ex-loading');
                            if(data.mes!=''){
                                jQuery('.exwf-del-address .exwf-add-error').html(data.mes).fadeIn();
                            }else{
                                jQuery( document.body ).trigger( 'wc_fragment_refresh' );
                                var url_cr = window.location.href;
                                if(jQuery('.exwf-del-log select.ex-logreq').length){ 
                                    if($loc!='' && $loc!=null){
                                        if (url_cr.indexOf("?") > -1){
                                            url_cr = url_cr+"&loc="+$loc;
                                        }else{
                                            url_cr = url_cr+"?loc="+$loc;
                                        }
                                    }
                                }else{
                                    //jQuery('.exwf-order-method').fadeOut();
                                }
                                url_cr = url_cr.replace("change-address=1","");
                                window.location = url_cr;
                                return false;
                            }
                        }else{jQuery('#'+id_crsc+' .loadmore-exfood').html('error');}
                    }
                });
            });
            jQuery('body').on('click', '.exwf-method-title > div', function (event) {
                jQuery('.exwf-method-title > div').removeClass('at-method');
                jQuery(this).addClass('at-method');
                if(jQuery(this).hasClass('exwf-order-take')){
                    jQuery('.exwf-del-address').fadeOut(); 
                }else{
                   jQuery('.exwf-del-address').fadeIn('fast');
                }
            });

        });
    </script>
    <div class="exwf-opcls-info exwf-odtype <?php esc_attr_e($class);?>">
        <div class="exwf-method-ct exwf-opcls-content">
            <div class="exwf-method-title">
                <?php if($method_ship!='takeaway'){?>
                    <div class="exwf-order-deli <?php if($user_odmethod!='takeaway' || $method_ship=='delivery'){?> at-method <?php }?>">
                        <?php esc_html_e('Delivery','woocommerce-food');?>
                    </div>
                    <?php 
                }
                if($method_ship!='delivery'){
                    ?>
                    <div class="exwf-order-take <?php if($user_odmethod=='takeaway' || $method_ship=='takeaway'){?> at-method <?php }?>">
                        <?php esc_html_e('Takeaway','woocommerce-food');?>
                    </div>
                <?php }?>
            </div>
            <div class="exwf-method-content">
                <?php if($loc_sl!=''){?>
                    <div class="exwf-del-field exwf-del-log">
                        <span><?php esc_html_e('Ordering area','woocommerce-food');?></span>
                        <div><?php echo $loc_sl; ?></div>
                        <p class="exwf-add-error"><?php esc_html_e('Please choose area you want to order','woocommerce-food');?></p>
                    </div>
                <?php }
                if($method_ship!='takeaway'){?>
                    <div class="exwf-del-field exwf-del-address" <?php if($user_odmethod=='takeaway'){?> style="display: none;" <?php } ?>>
                        <span><?php esc_html_e('Your delivery Location','woocommerce-food');?></span>
                        <div class="">
                            <input type="text" name="exwf-user-address" id="exwf-user-address" value="<?php echo $user_address!='' ? $user_address : ''; ?>">
                        </div>
                        <p class="exwf-add-error"><?php esc_html_e('Please add your address','woocommerce-food');?></p>
                    </div>
                <?php }?>
            </div>
            <div class="exwf-method-bt">
                <span class="exwf-button"><?php esc_html_e('Start my order','woocommerce-food');?></span>
            </div>
        </div>
    </div>
</div>