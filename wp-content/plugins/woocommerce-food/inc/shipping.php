<?php
// Get distance
$api = exwoofood_get_option('exwoofood_gg_api','exwoofood_shpping_options');
function exwf_get_distance($from,$to,$km){
	$rs =array();
	$map_lang = urlencode(apply_filters('exwf_map_matrix_lang','en-EN'));
	$api = exwoofood_get_option('exwoofood_gg_api','exwoofood_shpping_options');
	$diskm = exwoofood_get_option('exwoofood_restrict_km','exwoofood_shpping_options');
	if($km =='' ){ $km = $diskm;}
	if($api =='' || $km=='99999'){
		WC()->session->set( '_user_deli_adress' , $to);
		if($api !=''){
			$data_address = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=".esc_attr($api)."&address=".urlencode($to)."&language=$map_lang&sensor=true");
			$data_address = json_decode($data_address);
			if(isset($data_address->results[0]->address_components)){
				WC()->session->set( '_user_deli_adress_details' , $data_address->results[0]->address_components);
			}
		}else{
			
		}
		$rs['mes'] ='';
		return $rs;
	}
	$store_address = get_option( 'woocommerce_store_address', '' );
	if($from =='' ){ $from = $store_address;}

	$from = urlencode($from);
	$to = urlencode($to);
	$data = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?key=".esc_attr($api)."&origins=".$from."&destinations=".$to."&language=$map_lang&sensor=false");
	$data = json_decode($data);//print_r($data);exit;
	WC()->session->set( '_user_deli_adress' , '');
	WC()->session->set( '_user_deli_adress_details','');
	if(isset($data->rows[0])){
		$time = 0;
		$distance = 0;
		foreach($data->rows[0]->elements as $road) {
		    $time += $road->duration->value;
		    $distance += $road->distance->value;
		}
		if($distance<='0'){
			$rs['distance'] = '0';
			$rs['mes'] = esc_html__('Could not calculate distance to your address, please re-check your address','woocommerce-food');
		}else{
			$distance = $distance/1000;
			if($km!='' && $distance > $km){
				$rs['distance'] = $distance;
				$rs['limit'] = $km;
				$rs['mes'] = esc_html__('Your address are out of delivery zone, please change to carryout channel','woocommerce-food');;
			}else{
				if(isset($data->destination_addresses[0])){
					$data_address = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=".esc_attr($api)."&address=".$to."&language=$map_lang&sensor=true");
					$data_address = json_decode($data_address);
					//print_r($data_address);exit;
					if(isset($data_address->results[0]->address_components)){
						WC()->session->set( '_user_deli_adress_details' , $data_address->results[0]->address_components);
						// extract address to get street 
						/*foreach($data_address->results[0]->address_components as $address_component){
						    if(in_array('country', $address_component->types)){
						        $country = $address_component->long_name;
						        continue;
						    } elseif(in_array('route', $address_component->types)) {
						        $address = $address_component->long_name;
						        continue;
						    }
						    // etc...
						}*/
					}
					WC()->session->set( '_user_deli_adress' , $data->destination_addresses[0]);
				}
				$rs['distance'] = $distance;
				$rs['mes'] ='';
			}
		}
	}else{
		$rs['distance'] = 'null';
		$rs['mes'] = isset($data->error_message) ? $data->error_message : '';
	}
	return $rs;
}
// 
add_action( 'init', 'exwf_wc_session_user' );
function exwf_wc_session_user() {
    if ( is_user_logged_in() || is_admin() )
        return;

    if ( isset(WC()->session) && ! WC()->session->has_session() ) {
        WC()->session->set_customer_session_cookie( true );
    }
}


add_action( 'init','exwf_clear_user_address' );
function exwf_clear_user_address(){
	if(is_admin()&& !defined( 'DOING_AJAX' ) || !isset(WC()->session) ){ return;}
	if(isset($_GET['change-address']) && $_GET['change-address']==1){
		WC()->session->set( '_user_deli_adress' , '');
		WC()->session->set( '_user_deli_log' , '');
		if(exwoofood_loc_field_html()==''){
			WC()->session->set( '_user_order_method' , '');
		}
	}
	if(isset($_GET['change-method']) && ($_GET['change-method']=='delivery' || $_GET['change-method']=='takeaway' )){
		WC()->session->set( '_user_order_method' , $_GET['change-method']);
	}
	$method_ship = exwoofood_get_option('exwoofood_enable_method','exwoofood_shpping_options');
	if($method_ship=='takeaway'){
		WC()->session->set( '_user_order_method' , 'takeaway');
	}else if($method_ship=='delivery'){
		WC()->session->set( '_user_order_method' , 'delivery');
	}else if($method_ship==''){
		WC()->session->set( '_user_order_method' , '');
	}
}
// Popup order method
function exwoofood_loc_field_html($loc=false){
	$args = array(
		'hide_empty'        => true,
		'parent'        => '0',
	);
	if(exwoofood_get_option('exwoofood_enable_loc') !='yes'){
		$args['hide_empty'] = false; 
	}
	$locations = isset($loc) && $loc!='' ? explode(",",$loc) : array();
	if (!empty($locations) && !is_numeric($locations[0])) {
		$args['slug'] = $locations;
	}else if (!empty($locations)) {
		$args['include'] = $locations;
	}
	$terms = get_terms('exwoofood_loc', $args);
	ob_start();
	$loc_selected = isset($_SESSION['ex_userloc']) && $_SESSION['ex_userloc']!='' ? $_SESSION['ex_userloc'] :'';
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
		<select class="ex-ck-select exfd-choice-locate ex-logreq" name="_location">
			<?php 
			global $wp;
	    	$count_stop = 5;
	    	echo '<option disabled selected value>'.esc_html__( '-- Select --', 'woocommerce-food' ) .'</option>';
	    	foreach ( $terms as $term ) {
	    		$select_loc = '';
	    		if ($term->slug !='' && $term->slug == $loc_selected) {
	                $select_loc = ' selected="selected"';
	              }
		  		echo '<option value="'. esc_attr($term->slug) .'" '.$select_loc.'>'. wp_kses_post($term->name) .'</option>';
		  		echo exfd_show_child_location('',$term,$count_stop,$loc_selected,'yes');
		  	}
	        ?>
		</select>
		<?php
	}
	$loca = ob_get_contents();
	ob_end_clean();
	return $loca;
}

function exwf_poup_delivery_type_html($args){
	global $locations;
	$locations = isset($args['locations']) ? $args['locations'] : '';
	$method = WC()->session->get( '_user_order_method' );
	$user_log = WC()->session->get( '_user_deli_log' );
	$user_addre = WC()->session->get( '_user_deli_adress' );
	$loc_selected = isset($_SESSION['ex_userloc']) && $_SESSION['ex_userloc']!='' ? $_SESSION['ex_userloc'] :'';
	if($method=='takeaway' && (exwoofood_loc_field_html($locations)=='' || $loc_selected!='' ||  $user_log!='')){
		return;
	}
	if($user_addre!='' && exwoofood_get_option('exwoofood_enable_loc') !='yes' || ( $user_addre!='' && exwoofood_get_option('exwoofood_enable_loc') =='yes' && $loc_selected!='') ){ 
		return;
	}
	global $pu_del;
	if($pu_del==''){
		$pu_del = 1;
	}else{
		return;
	}
	
	exwoofood_template_plugin('order-method',1);
}
add_action('exwf_before_shortcode_content','exwf_poup_delivery_type_html',12,1);
// add shipping method to top of checkout field
add_action( 'woocommerce_before_checkout_form', 'exwf_shipping_method_selectbox' );
function exwf_shipping_method_selectbox(){
	// $customer_ss = WC()->session->get('customer');
	// $customer_ss['address_1'] = '1';//echo '<pre>';print_r($customer_ss);exit;
	// WC()->session->set('customer',$customer_ss);
	$method_ship = exwoofood_get_option('exwoofood_enable_method','exwoofood_shpping_options');
	if($method_ship==''){ return;}
	$user_odmethod = WC()->session->get( '_user_order_method' );
	global $wp;
	$cr_url =  home_url( $wp->request );
	?>
	<div class="exwf-cksp-method exwf-method-ct">
		<div class="exwf-method-title">
	        
	        <?php if($method_ship!='takeaway'){?>
	        	<a href="<?php echo esc_url(add_query_arg(array('change-method' => 'delivery' ), $cr_url));?>" class="exwf-order-deli <?php if($user_odmethod!='takeaway' || $method_ship=='delivery'){?> at-method <?php }?>">
		            <?php esc_html_e('Delivery','woocommerce-food');?>
		        </a>
                <?php 
            }
            if($method_ship!='delivery'){
                ?>
                <a href="<?php echo esc_url(add_query_arg(array('change-method' => 'takeaway' ), $cr_url));?>" class="exwf-order-take <?php if($user_odmethod=='takeaway' || $method_ship=='takeaway'){?> at-method <?php }?>">
		            <?php esc_html_e('Takeaway','woocommerce-food');?>
		        </a>
            <?php }?>
	    </div>
    </div>
	<?php
}



add_action( 'wp_ajax_exwf_check_distance', 'ajax_exwf_check_distance' );
add_action( 'wp_ajax_nopriv_exwf_check_distance', 'ajax_exwf_check_distance' );
function ajax_exwf_check_distance(){
	$method = $_POST['method'];
	$log = $_POST['log'];
	WC()->session->set( '_user_deli_log' , $log);
	WC()->session->set( '_user_order_method' , $method);
	$output = array();
	if($method=='takeaway'){
		$output['mes']= '';
	}else{
		$address = $_POST['address'];
		$from = '';
		$diskm = exwoofood_get_option('exwoofood_restrict_km','exwoofood_shpping_options');
		if($log!=''){
			$term = get_term_by('slug', $log, 'exwoofood_loc');
			if(isset($term->term_id)){
				$addres_log = get_term_meta( $term->term_id, 'exwp_loc_address', true );
				if($addres_log !=''){
					$from = $addres_log;
				}
				$addres_km = get_term_meta( $term->term_id, 'exwp_loc_diskm', true );
				if($addres_km !=''){
					$diskm = $addres_km;
				}
			}
		}
		$output = exwf_get_distance($from,$address,$diskm);
	}
	echo str_replace('\/', '/', json_encode($output));
	die;
}


add_action( 'exwf_sidecart_after_content', 'exwf_add_user_info_sidecart' );
function exwf_add_user_info_sidecart(){
	$method_ship = exwoofood_get_option('exwoofood_enable_method','exwoofood_shpping_options');
	if($method_ship==''){ return;}
	$user_odmethod = WC()->session->get( '_user_order_method' );
	global $wp;
	$cr_url =  home_url( $wp->request );
	$addres_log = '';
    if($user_odmethod=='takeaway'){
    	$user_log = WC()->session->get( '_user_deli_log' );
    	$url = add_query_arg(array('change-address' => 1), $cr_url);
    	if($user_log!=''){
			$term = get_term_by('slug', $user_log, 'exwoofood_loc');
			if(isset($term->term_id)){
				$addres_log = get_term_meta( $term->term_id, 'exwp_loc_address', true );
				if($addres_log ==''){
					$addres_log = $term->name;
				}
			}
		}else{
			$addres_log = get_option( 'woocommerce_store_address', '' );
		}?>
		<div class="exwf-user-dl-info">
            <span class="adrl-title"><?php esc_html_e('Carryout at: ','woocommerce-food'); ?></span>
            <span class="adrl-info"><?php echo $addres_log;?></span>
            <span class="adrl-link"><a href="<?php echo esc_url($url);?>"><?php esc_html_e(' Change it ?','woocommerce-food'); ?></a></span>
        </div>
		<?php
    }else{
        $user_address = WC()->session->get( '_user_deli_adress' );
        if($user_address!=''){
            $url = add_query_arg(array('change-address' => 1), $cr_url);?>
            <div class="exwf-user-dl-info">
                <span class="adrl-title"><?php esc_html_e('Delivery to: ','woocommerce-food'); ?></span>
                <span class="adrl-info"><?php echo $user_address;?></span>
                <span class="adrl-link"><a href="<?php echo esc_url($url);?>"><?php esc_html_e(' Change it ?','woocommerce-food'); ?></a></span>
            </div>
        <?php }
    }
}
// Verify address checkout
add_action('woocommerce_checkout_process', 'exwf_verify_address_deli_field');

function exwf_verify_address_deli_field() {
	$user_odmethod = WC()->session->get( '_user_order_method' );
	if($user_odmethod=='takeaway'){
		return;
	}
	$loc_sl = isset($_POST['exwoofood_ck_loca']) ? $_POST['exwoofood_ck_loca'] : '';
	$to = isset($_POST['billing_address_1']) ? $_POST['billing_address_1'] : '';
	$diskm ='';
	if($loc_sl!=''){
		$term = get_term_by('slug', $loc_sl, 'exwoofood_loc');
		if(isset($term->term_id)){
			$addres_log = get_term_meta( $term->term_id, 'exwp_loc_address', true );
			if($addres_log !=''){
				$from = $addres_log;
			}
			$addres_km = get_term_meta( $term->term_id, 'exwp_loc_diskm', true );
			if($addres_km !=''){
				$diskm = $addres_km;
			}
		}
	}
	$output = exwf_get_distance($from,$to,$diskm);
	if($output['mes']!=''){
		wc_add_notice( $output['mes'], 'error' );
	}
}
// add shipping fee
add_action( 'woocommerce_cart_calculate_fees','exwd_add_shipping_fee' );
function exwd_add_shipping_fee() {
	$user_odmethod = WC()->session->get( '_user_order_method' );
	if($user_odmethod=='takeaway'){
		return;
	}
	$fee = exwoofood_get_option('exwoofood_ship_fee','exwoofood_shpping_options');
	if($fee!='' && is_numeric($fee)){
	  	global $woocommerce;
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ){
			return;	
		}
		$free_shipping = exwoofood_get_option('exwoofood_ship_free','exwoofood_shpping_options');
		$total = apply_filters( 'exwf_total_cart_price_fee', WC()->cart->get_subtotal() );
		if($free_shipping!='' && is_numeric($free_shipping) && $total>=$free_shipping){
			$fee = 0;
		}
		$woocommerce->cart->add_fee( esc_html__('Shipping fee','woocommerce-food'), $fee, true, '' );
	}
}
// display shipping free
add_action( 'woocommerce_widget_shopping_cart_before_buttons' , 'exwf_minimum_amount_free_deli_sidecart',999 );
function exwf_minimum_amount_free_deli_sidecart($return = false){
	$user_odmethod = WC()->session->get( '_user_order_method' );
	if($user_odmethod=='takeaway'){	
		return;
	}
	$free_shipping = exwoofood_get_option('exwoofood_ship_free','exwoofood_shpping_options');
	$fee = exwoofood_get_option('exwoofood_ship_fee','exwoofood_shpping_options');
	$total = apply_filters( 'exwf_total_cart_price_fee', WC()->cart->get_subtotal() );
	// min by log
	$html ='';
    if ( $fee!='' && $free_shipping!='' && is_numeric($fee) && is_numeric($free_shipping) && $total < $free_shipping ) {
    	$html = sprintf( esc_html__('Order %s more to get free delivery','woocommerce-food' ) , wc_price( $free_shipping ));
    }
    if($html!='' && isset($return) && $return==true){
    	return $html;
    }else if($html!=''){
    	echo '<p class="exwf-mini-amount exwf-warning">'.$html.'</p>';
    }
}
add_action( 'woocommerce_before_cart' , 'exwf_minimum_amount_fee_deli' );
function exwf_minimum_amount_fee_deli() {
	$user_odmethod = WC()->session->get( '_user_order_method' );
	if($user_odmethod=='takeaway'){	
		return;
	}
	wc_print_notice(exwf_minimum_amount_free_deli_sidecart(true), 'error');
}
/**
 * Save method value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'exwf_save_order_method_field' );

function exwf_save_order_method_field( $order_id ) {
	$user_odmethod = WC()->session->get( '_user_order_method' );
	update_post_meta( $order_id, 'exwfood_order_method', $user_odmethod );
}
//
add_filter( 'woocommerce_default_address_fields' , 'exwf_unrequired_address_field' );
function exwf_unrequired_address_field( $address_fields ) {
    $user_odmethod = WC()->session->get( '_user_order_method' );
	if($user_odmethod=='takeaway'){	
	    $address_fields['address_1']['required'] = false;
	}

     return $address_fields;
}

add_filter( 'default_checkout_billing_country', 'exwf_change_default_checkout_country' );
//add_filter( 'default_checkout_billing_state', 'exwf_change_default_checkout_state' );
add_filter( 'default_checkout_billing_city', 'exwf_change_default_checkout_city' );
add_filter( 'default_checkout_billing_address_1', 'exwf_change_default_checkout_address_1' );
add_filter( 'default_checkout_billing_postcode', 'exwf_change_default_checkout_billing_postcode' );
function exwf_change_default_checkout_country($contr) {
	$user_address = WC()->session->get( '_user_deli_adress' );
	$user_details_address = WC()->session->get( '_user_deli_adress_details' );//print_r($user_details_address);exit;
	if($user_details_address!='' && is_array($user_details_address)){
		foreach($user_details_address as $address_component){
		    if(in_array('country', $address_component->types)){
		        $country = $address_component->short_name;
		        return $country;
		        break;
		    } 
		}
	}
	if($user_address!=''){
		$user_address = explode(",",$user_address);
		$contr = end($user_address);
		$contr = exwf_get_country_code(trim($contr));
	}
	return $contr; // country code
}

function exwf_change_default_checkout_state() {
  return 'US'; // state code
}

function exwf_change_default_checkout_city($city) {
	$user_address = WC()->session->get( '_user_deli_adress' );
	$user_details_address = WC()->session->get( '_user_deli_adress_details' );//print_r($user_details_address);exit;
	if($user_details_address!='' && is_array($user_details_address)){
		foreach($user_details_address as $address_component){
		    if(in_array('locality', $address_component->types)){
		        $name = $address_component->long_name;
		        return $name;
		        break;
		    } 
		}
	}
	if($user_address!=''){
		$user_address = explode(",",$user_address);
		$count_ar = count($user_address);
		if($count_ar > 1){//print_r($user_address);exit;
			$city = $user_address[$count_ar-2];
		}
	}
	return $city; // state code
}

function exwf_change_default_checkout_address_1($street) {
	$user_address = WC()->session->get( '_user_deli_adress' );
	$user_details_address = WC()->session->get( '_user_deli_adress_details' );//print_r($user_details_address);exit;
	if($user_details_address!='' && is_array($user_details_address)){
		$name ='';
		foreach($user_details_address as $address_component){
		    if(in_array('street_number', $address_component->types)){
		        $name .= $address_component->long_name;
		    }else if(in_array('route', $address_component->types)){
		        $name .= ' '.$address_component->long_name;
		    }else if(in_array('neighborhood', $address_component->types)){
		        $name .= ' '.$address_component->long_name;
		    }
		}
		return $name;
	}
	if($user_address!=''){
		$user_address = explode(",",$user_address);
		$count_ar = count($user_address);
		if($count_ar > 3){
			$street = '';
			for($i= 0; $i < ($count_ar-2);$i ++ ){
				$street .= ' '.$user_address[$i];
			}
		}
	}
	return $street; // state code
}

function exwf_change_default_checkout_billing_postcode($code) {
	$user_details_address = WC()->session->get( '_user_deli_adress_details' );//print_r($user_details_address);exit;
	if($user_details_address!='' && is_array($user_details_address)){
		foreach($user_details_address as $address_component){
		    if(in_array('postal_code', $address_component->types)){
		        $name = $address_component->long_name;
		        return $name;
		        break;
		    } 
		}
	}
	return $code; // state code
}

function exwf_get_country_code($name){
	$countrycodes = array (
		'AF' => 'Afghanistan','AX' => 'Åland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua and Barbuda', 'AR' => 'Argentina', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan', 'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium', 'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia', 'BA' => 'Bosnia and Herzegovina', 'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam', 'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada', 'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China', 'CX' => 'Christmas Island', 'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo', 'CD' => 'Zaire', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => 'Côte D\'Ivoire', 'HR' => 'Croatia', 'CU' => 'Cuba', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia', 'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana', 'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam', 'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti', 'HM' => 'Heard Island and Mcdonald Islands', 'VA' => 'Vatican City State', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle of Man', 'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'KENYA', 'KI' => 'Kiribati', 'KP' => 'Korea, Democratic People\'s Republic of', 'KR' => 'Korea, Republic of', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia', 'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libyan Arab Jamahiriya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia, the Former Yugoslav Republic of', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States of', 'MD' => 'Moldova, Republic of', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro', 'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands', 'AN' => 'Netherlands Antilles', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NI' => 'Nicaragua', 'NE' => 'Niger', 'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman', 'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar', 'RE' => 'Réunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'SH' => 'Saint Helena', 'KN' => 'Saint Kitts and Nevis', 'LC' => 'Saint Lucia', 'PM' => 'Saint Pierre and Miquelon', 'VC' => 'Saint Vincent and the Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome and Principe', 'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia and the South Sandwich Islands', 'ES' => 'Spain', 'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard and Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan, Province of China', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania, United Republic of', 'TH' => 'Thailand', 'TL' => 'Timor-Leste', 'TG' => 'Togo', 'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad and Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan', 'TC' => 'Turks and Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom', 'US' => 'United States', 'UM' => 'United States Minor Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela', 'VN' => 'Vietnam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.', 'WF' => 'Wallis and Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe',
	);
	$code = array_search($name, $countrycodes);
	return $code;
}