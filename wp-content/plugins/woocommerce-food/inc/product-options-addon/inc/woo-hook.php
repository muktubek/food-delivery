<?php
/**
 * Get options
 */
function exwoo_get_options($id){
	if($id ==''){ $id = get_the_ID();}
	//Check global option
	$cate = wp_get_post_terms($id,'product_cat',array( 'fields' => 'slugs' ));
	$args = array(
		'post_type'     => 'exwo_glboptions',
		'post_status'   => array( 'publish' ),
		'numberposts'   => -1,
		'suppress_filters' => true
	);
	if(!empty($cate) && count($cate) > 0){
		$args['tax_query'] = array(
				array(
				'taxonomy'         => 'product_cat',
				'field'            => 'slug',
				'terms'            => $cate,
				'operator' => 'IN',
				'include_children'=>false,
			)
		);
	}
	$glb_otqr = get_posts( $args );
	$global_op = array();
	if(!empty($glb_otqr) && count($glb_otqr) > 0){
		foreach ($glb_otqr as $op_item) {
			$goptions = get_post_meta( $op_item->ID, 'exwo_options', true );
			$global_op = array_merge($global_op,$goptions);
		}
	}
	//$glb_options = 
	$data_options = get_post_meta( $id, 'exwo_options', true );
	if(!empty($global_op)){
		if($data_options==''){$data_options=array();}
		$data_options = array_merge($data_options,$global_op);
	}
	return $data_options;
}
/**
 * Add the field to add to cart form
 */
function exwoo_display_custom_field() {
	global $post;
	$text_domain = exwo_text_domain();
	// Check for the custom field value
	$data_options = exwoo_get_options($post->ID);//echo '<pre>'; print_r($data_options);exit;
	if(is_array($data_options) && !empty($data_options)){
		$i = 0;
		$show_more = apply_filters( 'exwo_show_more_option_button', 0 );
		$cls = $show_more=='1' ? 'exwo-hide-options' : '';
		$accordion_style = apply_filters( 'exwo_accordion_style', 0 );
		$cls = $accordion_style=='1' ? 'exwo-accordion-style' : '';
		echo '<div class="exwo-product-options '.esc_attr($cls).'">';
		foreach ($data_options as $item) {
			$type = isset($item['_type']) && $item['_type']!='' ? $item['_type'] : 'checkbox';
			$required = isset($item['_required']) && $item['_required']!='' ? 'ex-required' : '';
			$min_req = $required_m = '';
			if($type=='checkbox'){
				$min_req = isset($item['_min_op']) && $item['_min_op']!='' ? $item['_min_op'] : '';
				if(is_numeric($min_req) && $min_req > 0){
					$required_m =' ex-required-min';
				}
			}
			echo '<div class="exrow-group ex-'.esc_attr($type).' '.esc_attr($required).' '.esc_attr($required_m).'" data-minsl="'.$min_req.'">';
			if($item['_name']){
				$price_tt = '';
				if($type =='text' || $type =='textarea'){
					$price_tt = isset($item['_price']) && $item['_price']!='' ? wc_price($item['_price']) :'';
					$price_tt = $price_tt !='' ? '<span> + '.wp_strip_all_tags($price_tt).'</span>' : '';
				}
				echo  '<span class="exfood-label"><span class="exwo-otitle">'.$item['_name'].'</span> '.$price_tt.'</span>' ;
			}
			echo '<div class="exwo-container">';
			$options = isset($item['_options']) ? $item['_options'] : '';
			if($type =='radio' && !empty($options)){
				foreach ($options as $key => $value) {
					$op_name = isset($value['name'])? $value['name'] : '';
					$op_val = isset($value['price'])? $value['price'] : '';
					$op_typ = isset($value['type'])? $value['type'] : '';
					$op_name = $op_val !='' ? $op_name .' + '.wc_price($op_val) : $op_name;
					echo '<span><input class="ex-options" type="radio" name="ex_options_'.esc_attr($i).'[]" value="'.esc_attr($key).'" data-price="'.esc_attr($op_val).'" data-type="'.esc_attr($op_typ).'">'.wp_kses_post($op_name).'</span>';
				}
			}else if($type =='select' && !empty($options)){
				echo '<select class="ex-options" name="ex_options_'.esc_attr($i).'[]">';
				echo '<option value="" data-price="">'.esc_html__( 'Select', $text_domain ).'</option>';
				foreach ($options as $key => $value) {
					$op_name = isset($value['name'])? $value['name'] : '';
					$op_val = isset($value['price'])? $value['price'] : '';
					$op_typ = isset($value['type'])? $value['type'] : '';
					$op_name = $op_val !='' ? $op_name .' + '.wc_price($op_val) : $op_name;
					echo '<option value="'.esc_attr($key).'" data-price="'.esc_attr($op_val).'" data-type="'.esc_attr($op_typ).'">'.wp_kses_post($op_name).'</option>';
				}
				echo '<select>';
			}else if($type =='text'){
				$price_ta = isset($item['_price']) && $item['_price']!='' ? $item['_price'] :'';
				$price_typ = isset($item['_price_type']) && $item['_price_type']!='' ? $item['_price_type'] :'';
				echo '<input class="ex-options" type="text" name="ex_options_'.esc_attr($i).'" data-price="'.esc_attr($price_ta).'" data-type="'.esc_attr($price_typ).'"/>';
			}else if($type =='textarea'){
				$price_ta = isset($item['_price']) && $item['_price']!='' ? $item['_price'] :'';
				$price_typ = isset($item['_price_type']) && $item['_price_type']!='' ? $item['_price_type'] :'';
				echo '<textarea class="ex-options" name="ex_options_'.esc_attr($i).'" data-price="'.esc_attr($price_ta).'" data-type="'.esc_attr($price_typ).'"/></textarea>';
			}else if(!empty($options)){
				foreach ($options as $key => $value) {
					$op_name = isset($value['name'])? $value['name'] : '';
					$op_val = isset($value['price'])? $value['price'] : '';
					$op_typ = isset($value['type'])? $value['type'] : '';
					$op_name = $op_val !='' ? $op_name .' + '.wc_price($op_val) : $op_name;
					echo '<span><input class="ex-options" type="checkbox" name="ex_options_'.esc_attr($i).'[]" value="'.esc_attr($key).'" data-price="'.esc_attr($op_val).'" data-type="'.esc_attr($op_typ).'">'.wp_kses_post($op_name).'</span>';
				}
			}
			if($required!=''){
				echo '<p class="ex-required-message">'.esc_html__('This option is required', $text_domain ).'</p>';
			}
			if($type=='checkbox' && is_numeric($min_req) && $min_req > 0){
				echo '<p class="ex-required-min-message">'.sprintf( esc_html__('Please choose at least %s options.','woocommerce-food' ) , $min_req).'</p>';
			}
			echo '</div>
			</div>';
			$i ++;
		}
		do_action( 'exwo_after_product_options');
		echo '</div>';
		if($show_more =='1'){
			echo '<div class="exwo-showmore"><span>'.esc_html__( 'Show extra options', $text_domain ).'<span></div>';
		}

	}
}
add_action( 'woocommerce_before_add_to_cart_button', 'exwoo_display_custom_field' );
/**
 * Validate the text field
 */
function exwo_validate_custom_field( $passed, $product_id, $quantity ) {
	$data_options = exwoo_get_options($product_id);
	$text_domain = exwo_text_domain();
	$msg = '';
	if(is_array($data_options) && !empty($data_options)){
		foreach ( $data_options as $key=> $options ) {
			$rq = isset($options['_required']) ? $options['_required'] : ''; 
			$data_exts = isset($_POST['ex_options_'.$key]) ? $_POST['ex_options_'.$key] :'';
			if(is_array($data_exts) && count($data_exts) ==1 && $data_exts[0]==''){
				$data_exts = '';
			}
			$type = isset($options['_type']) && $options['_type']!='' ? $options['_type'] : 'checkbox';
			$min_req = $type=='checkbox' && isset($options['_min_op']) && $options['_min_op']!='' ? $options['_min_op'] : 0;
			$c_item = !empty($data_exts) && is_array($data_exts) ? count($data_exts) : 0;
			if( ($rq =='yes' && ($data_exts=='' || empty($data_exts))) || ( $min_req > 0 &&  $min_req > $c_item)  ){
				$passed = false;
				wc_add_notice( __( 'Please re-check all required fields and try again', $text_domain ), 'error' );
				break;
			}
			
		}
	}
	return $passed;
}
add_filter( 'woocommerce_add_to_cart_validation', 'exwo_validate_custom_field', 10, 3 );


/**
 * Add the text field as item data to the cart object
 */
function exwo_add_custom_field_item_data( $cart_item_data, $product_id ) {

	$data_options = exwoo_get_options($product_id);
	$c_options = array();
	//$price = '';
	if(is_array($data_options) && !empty($data_options)){
		foreach ( $data_options as $key=> $options ) {
			$data_exts = isset($_POST['ex_options_'.$key]) ? $_POST['ex_options_'.$key] :'';
			if(isset($options['_type']) &&($options['_type']=='text' || $options['_type']=='textarea')){
				$price_op = isset($options['_price']) ? $options['_price'] : '';
				if($data_exts!=''){
					$type_price = isset($options['_price_type']) ? $options['_price_type'] : '';
					$c_options[] = array(
						'name'       => sanitize_text_field( $options['_name'] ),
						'value'      => $data_exts,
						'type_of_price'      => $type_price,
						'price'      => floatval($price_op),
					);
					//$price += (float) floatval($price_op);
				}
			}else{
				if(is_array($data_exts) && !empty($data_exts)){
					foreach ($data_exts as $value) {
						if($value!=''){
							$price_op = isset($options['_options'][$value]['price']) ? $options['_options'][$value]['price'] : '';
							$type_price = isset($options['_options'][$value]['type']) ? $options['_options'][$value]['type'] : '';
							$c_options[] = array(
								'name'       => sanitize_text_field( $options['_name'] ),
								'value'      => $options['_options'][$value]['name'],
								'type_of_price'      => $type_price,
								'price'      => floatval($price_op),
							);
							//$price += (float) floatval($price_op);
						}
					}
				}
			}
		}
		$cart_item_data['exoptions'] = $c_options;
	}
	return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'exwo_add_custom_field_item_data', 10, 2 );
/**
 * Update price
 */
add_filter( 'woocommerce_add_cart_item',  'exwf_update_total_price_item', 30, 1 );
function exwf_update_total_price_item($cart_item){
	if(isset($cart_item['exoptions']) && is_array($cart_item['exoptions'])){
		$price = (float) $cart_item['data']->get_price( 'edit' );
		$qty = $cart_item['quantity'];
		foreach ( $cart_item['exoptions'] as $option ) {
			if ( $option['price'] ) {
				if($option['type_of_price'] == 'fixed'){
					$price += (float) $option['price']/$qty;
				}else{
					$price += (float) $option['price'];
				}
			}
		}
		$cart_item['data']->set_price( $price );
	}
	return $cart_item;
}
add_filter( 'woocommerce_get_cart_item_from_session', 'exwf_update_total_from_session', 20, 2 );
function exwf_update_total_from_session($cart_item, $values){
	if(isset($cart_item['exoptions']) && is_array($cart_item['exoptions'])){
		$cart_item = exwf_update_total_price_item($cart_item);
	}
	return $cart_item;
}
/**
 * Display in cart
 */
add_filter('woocommerce_get_item_data','exwf_show_option_in_cart',1,2);
function exwf_show_option_in_cart( $other_data, $cart_item ) {
	if(isset($cart_item['exoptions']) && is_array($cart_item['exoptions'])){
		$wt_date_label = get_post_meta( $cart_item['product_id'], 'wt_date_label', true );
		$show_sgline = apply_filters( 'exwf_show_options_single_line', 'no' );
		if($show_sgline!='yes'){
			foreach ( $cart_item['exoptions'] as $option ) {
				$price_s = isset($option['price']) && $option['price']!='' ? $option['value'] .' + '.wc_price($option['price']) : $option['value'];
				$price_s = apply_filters( 'exwo_price_show_inorder', $price_s, $option );
				$other_data[] = array(
					'name'  => $option['name'],
					'value' => $price_s
				);
			}
		}else{
			$grouped_types = array();
			foreach($cart_item['exoptions'] as $type){
			    $grouped_types[$type['name']][] = $type;
			}
			foreach ($grouped_types as $key => $option_tp) {
				if (is_array($option_tp)){
					$price_a = '';
					$i = 0;
					foreach ($option_tp as $option_it) {
						$i ++;
						$name = $option_it['name'];
						$price_s = isset($option_it['price']) && $option_it['price']!='' ? $option_it['value'] .' + '.wc_price($option_it['price']) : $option_it['value'];
						$price_s = apply_filters( 'exwo_price_show_inorder', $price_s, $option_it );
						$price_a .= $price_s;
						if($i > 0 && $i < count($option_tp)){$price_a .=', '; }
					}
					$other_data[] = array(
						'name'  => $option_it['name'],
						'value' => $price_a
					);
				}
			}
		}
	}
	return $other_data;
}
/**
 * Add option to order object
 */
function exwf_add_options_to_order( $item, $cart_item_key, $values, $order ) {
	if(isset($values['exoptions']) && is_array($values['exoptions'])){
		foreach ( $values['exoptions'] as $option ) {
			$value = strip_tags($option['value'] .' + '.wc_price($option['price']));
			$price_s = apply_filters( 'exwo_price_show_inorder', $value, $option );
			$item->add_meta_data( $option['name'], $price_s );
		}
	}
}
add_action( 'woocommerce_checkout_create_order_line_item', 'exwf_add_options_to_order', 10, 4 );