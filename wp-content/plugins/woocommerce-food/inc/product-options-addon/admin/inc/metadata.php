<?php
/**
 * Register metadata box
 */
add_action( 'cmb2_admin_init', 'exwo_register_metabox' );

function exwo_register_metabox() {
	$prefix = 'exwo_';
	$text_domain = exwo_text_domain();
	/**
	 * Food general info
	 */
	$exwo_options = new_cmb2_box( array(
		'id'            => $prefix . 'addition_options',
		'title'         => esc_html__( 'Additional option', $text_domain ),
		'object_types'  => array( 'product','exwo_glboptions' ), // Post type
	) );
	$group_option = $exwo_options->add_field( array(
		'id'          => $prefix . 'options',
		'type'        => 'group',
		'description' => esc_html__( 'Add additional product option to allow user can order with this product', $text_domain ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
			'group_title'   => esc_html__( 'Option {#}', $text_domain ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Option', $text_domain ),
			'remove_button' => esc_html__( 'Remove Option', $text_domain ),
			'sortable'      => true, // beta
			'closed'     => true, // true to have the groups closed by default
		),
		'after_group' => '',
	) );
	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Name', $text_domain ),
		'id'   => '_name',
		'type' => 'text',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Option type', $text_domain ),
		'description' => esc_html__( 'Select type of this option', $text_domain ),
		'id'   => '_type',
		'classes' => 'extype-option',
		'type' => 'select',
		'show_option_none' => false,
		'default' => '',
		'options'          => array(
			'' => esc_html__( 'Checkboxes', $text_domain ),
			'radio'   => esc_html__( 'Radio buttons', $text_domain ),
			'select'   => esc_html__( 'Select box', $text_domain ),
			'text'   => esc_html__( 'Textbox', $text_domain ),
			'textarea'   => esc_html__( 'Textarea', $text_domain ),
		),
	) );
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Required?', $text_domain ),
		'description' => esc_html__( 'Select this option is required or not', $text_domain ),
		'id'   => '_required',
		'type' => 'select',
		'show_option_none' => false,
		'default' => '',
		'options'          => array(
			'' => esc_html__( 'No', $text_domain ),
			'yes'   => esc_html__( 'Yes', $text_domain ),
		),
	) );
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Minimun selection', $text_domain ),
		'classes' => 'exhide-radio exhide-select exhide-textbox exhide-textarea',
		'description' => esc_html__( 'Enter number minimum at least option required', $text_domain ),
		'id'   => '_min_op',
		'type' => 'text',
		'default' => '',
	) );
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Options', $text_domain ),
		'classes' => 'exhide-textbox exhide-textarea',
		'description' => esc_html__( 'Set name and price for each option', $text_domain ),
		'id'   => '_options',
		'type' => 'price_options',
		'repeatable'     => true,
	) );
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Type of price', $text_domain ),
		'description' => '',
		'classes' => 'exshow-textbox exshow-textarea exwo-hidden',
		'id'   => '_price_type',
		'type' => 'select',
		'show_option_none' => false,
		'default' => '',
		'options'          => array(
			'' => esc_html__( 'Quantity Based', $text_domain ),
			'fixed'   => esc_html__( 'Fixed Amount', $text_domain ),
		),
	) );
	$exwo_options->add_group_field( $group_option, array(
		'name' => esc_html__( 'Price', $text_domain ),
		'classes' => 'exshow-textbox exshow-textarea exwo-hidden',
		'description' => '',
		'id'   => '_price',
		'type' => 'text',
		'default' => '',
	) );
}
// Metadata repeat field
function exwocmb2_get_price_type_options( $text_domain,$value = false ) {
	$_list = array(
		''   => esc_html__( 'Quantity Based', $text_domain ),
		'fixed' => esc_html__( 'Fixed Amount', $text_domain ),
	);

	$_options = '';
	foreach ( $_list as $abrev => $state ) {
		$_options .= '<option value="'. $abrev .'" '. selected( $value, $abrev, false ) .'>'. $state .'</option>';
	}

	return $_options;
}

function exwocmb2_render_price_options_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
	$text_domain = exwo_text_domain();
	// make sure we specify each part of the value we need.
	$value = wp_parse_args( $value, array(
		'name' => '',
		'type' => '',
		'price' => '',
	) );
	?>
	<div class="exwo-options exwo-name-option"><p><label for="<?php echo $field_type->_id( '_name' ); ?>"><?php esc_html_e('Option name',$text_domain)?></label></p>
		<?php echo $field_type->input( array(
			'class' => '',
			'name'  => $field_type->_name( '[name]' ),
			'id'    => $field_type->_id( '_name' ),
			'value' => $value['name'],
			'type'  => 'text',
			'desc'  => '',
		) ); ?>
	</div>
	<div class="exwo-options exwo-price-option"><p><label for="<?php echo $field_type->_id( '_price' ); ?>'"><?php esc_html_e('Price',$text_domain)?></label></p>
		<?php echo $field_type->input( array(
			'class' => '',		
			'name'  => $field_type->_name( '[price]' ),
			'id'    => $field_type->_id( '_price' ),
			'value' => $value['price'],
			'type'  => 'text',
			'desc'  => '',
		) ); ?>
	</div>
	<div class="exwo-options exwo-type-option"><p><label for="<?php echo $field_type->_id( '_type' ); ?>'"><?php esc_html_e('Type of price',$text_domain)?></label></p>
		<?php echo $field_type->select( array(
			'class' => '',		
			'name'  => $field_type->_name( '[type]' ),
			'id'    => $field_type->_id( '_type' ),
			'value' => $value['type'],
			'options' => exwocmb2_get_price_type_options($text_domain, $value['type'] ),
			'desc'  => '',
		) ); ?>
	</div>
	<br class="clear">
	<?php
	echo $field_type->_desc( true );

}
add_filter( 'cmb2_render_price_options', 'exwocmb2_render_price_options_field_callback', 10, 5 );
function exwocmb2_sanitize_price_options_callback( $override_value, $value ) {
	echo '<pre>';print_r($value);exit;
	return $value;
}
//add_filter( 'cmb2_sanitize_openclose', 'exwocmb2_sanitize_price_options_callback', 10, 2 );


add_filter( 'cmb2_sanitize_price_options', 'exwosanitize' , 10, 5 );
add_filter( 'cmb2_types_esc_price_options', 'exwoescape' , 10, 4 );
function exwosanitize( $check, $meta_value, $object_id, $field_args, $sanitize_object ) {

	// if not repeatable, bail out.
	if ( ! is_array( $meta_value ) || ! $field_args['repeatable'] ) {
		return $check;
	}

	foreach ( $meta_value as $key => $val ) {
		$meta_value[ $key ] = array_filter( array_map( 'sanitize_text_field', $val ) );
	}

	return array_filter( $meta_value );
}

function exwoescape( $check, $meta_value, $field_args, $field_object ) {
	// if not repeatable, bail out.
	if ( ! is_array( $meta_value ) || ! $field_args['repeatable'] ) {
		return $check;
	}

	foreach ( $meta_value as $key => $val ) {
		$meta_value[ $key ] = array_filter( array_map( 'esc_attr', $val ) );
	}

	return array_filter( $meta_value );
}
