;(function($){
	'use strict';
	$(document).ready(function() {
		function exwo_add_title($box){
			if(!$box.length){ return;}
			$box.find( '.cmb-group-title' ).each( function() {
				var $this = $( this );
				var txt = $this.next().find( '[id$="_name"]' ).val();
				var rowindex;
				if ( ! txt ) {
					txt = $box.find( '[data-grouptitle]' ).data( 'grouptitle' );
					if ( txt ) {
						rowindex = $this.parents( '[data-iterator]' ).data( 'iterator' );
						txt = txt.replace( '{#}', ( rowindex + 1 ) );
					}
				}
				if ( txt ) {
					$this.text( txt );
				}
			});
		}
		function exwo_replace_title(evt){
			var $this = $( evt.target );
			var id = 'name';
			if ( evt.target.id.indexOf(id, evt.target.id.length - id.length) !== -1 ) {
				$this.parents( '.cmb-row.cmb-repeatable-grouping' ).find( '.cmb-group-title' ).text( $this.val() );
			}
		}
		jQuery('#exwo_addition_options').on( 'cmb2_add_row cmb2_shift_rows_complete', exwo_add_title )
				.on( 'keyup', exwo_replace_title );
		exwo_add_title(jQuery('#exwo_addition_options'));
		jQuery('body').on('click', function() {
			exwo_add_title(jQuery('#exwo_addition_options'));
		});

		// show hide option by type of option
		if(jQuery('#cmb2-metabox-exwo_addition_options.cmb2-metabox .extype-option select').length>0){
			jQuery('#cmb2-metabox-exwo_addition_options.cmb2-metabox .extype-option select').each(function(){
				var $this = $(this);
				var $val = $this.val();
				if($val!=''){
					$this.closest('.postbox.cmb-repeatable-grouping').addClass('ex-otype-'+$val);
				}
			});
			jQuery('body').on('change', '#cmb2-metabox-exwo_addition_options.cmb2-metabox .extype-option select', function() {
				var $this = $(this);
				var $val = $this.val();
				$this.closest('.postbox.cmb-repeatable-grouping').removeClass (function (index, className) {
					return (className.match (/(^|\s)ex-otype\S+/g) || []).join(' ');
				});
				if($val!=''){
					$this.closest('.postbox.cmb-repeatable-grouping').addClass('ex-otype-'+$val);
				}
			});
		}
    });
}(jQuery));