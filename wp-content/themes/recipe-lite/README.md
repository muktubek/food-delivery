# Recipe Lite

**Contributors:** sonalsinha21
**Requires at least:** 5.0
**Requires PHP:**  5.6
**Tested up to:** 5.5
**Version:** 1.2
**License:** GPL-2.0-or-later
**License URI:** https://www.gnu.org/licenses/license-list.html#GPLv2
**Tags:** two-columns,right-sidebar,custom-background,custom-colors,custom-menu,sticky-post,theme-options,threaded-comments

## Description

SKT Recipe Lite WordPress theme can be used by chefs, recipe makers, recipe and food bloggers, caterers, restaurant and bistro owners, cafe and coffee shops as well as other food packaging owners to have a nice and decent website. Pizza delivery and online food ordering website can also use this template and make use of WooCommerce for online food delivery and booking orders. Translation ready and page builder friendly this recipe template is also compatible with multilingual plugins and recipe plugins. It is multipurpose template and comes with a ready to import Elementor template plugin as add on which allows to import 63+ design templates for making use in home and other inner pages. Use it to create any type of business, personal, blog and eCommerce website. It is fast, flexible, simple and fully customizable.
 
## Theme Resources

Theme is built using the following resource bundles:

1 - All js that have been used are within folder /js of theme.

2 -     jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com
MIT License URL: https://opensource.org/licenses/MIT

3 - Roboto Condensed - https://www.google.com/fonts/specimen/Roboto+Condensed
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://www.apache.org/licenses/LICENSE-2.0.html
	
  - Roboto - https://fonts.google.com/specimen/Roboto
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://www.apache.org/licenses/LICENSE-2.0.html	
	
  - Great Vibes - https://fonts.google.com/specimen/Great+Vibes
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web	
	
4 - SKT Themes Self Designed Images:
	recipe-lite/images/free-vs-pro.png
	recipe-lite/images/icon1.png
	recipe-lite/images/icon2.png
	recipe-lite/images/icon3.png
	recipe-lite/images/icon4.png
    recipe-lite/images/icon-gp.png
    recipe-lite/images/icon-fb.png
	recipe-lite/images/icon-in.png
	recipe-lite/images/icon-tw.png
	recipe-lite/images/loading.gif
	recipe-lite/images/mobile_nav_right.png
	recipe-lite/images/sktskill.png
	recipe-lite/images/slide-nav.png
		
Declaring these self designed images under GPL license version 2.0
License URL: http://www.gnu.org/licenses/gpl-2.0.html
		
5 -     recipe-lite/customize-pro	

		Customize Pro code based on Justintadlock’s Customizer-Pro 
		Source Code URL : https://github.com/justintadlock/trt-customizer-pro			
		License : http://www.gnu.org/licenses/gpl-2.0.html
		Copyright 2016, Justin Tadlock	justintadlock.com
		
6 -     Details of images used in Screenshot:
		
		Below Slider Three Boxes Image Are Maxpixel Images:
        
		https://www.maxpixel.net/Food-Hamburger-Burger-Meat-Lunch-Meal-2573682
		https://www.maxpixel.net/French-Fries-Food-Potato-Fries-Fries-Macro-Bowl-1842294
		https://www.maxpixel.net/Fruits-Salad-Cream-Nuts-Pomelo-Mayo-Raisins-250871
  	
		
		Declaring these self designed images under GPL license version 2.0
		License URL: http://www.gnu.org/licenses/gpl-2.0.html
		
		Slider Images : 
		recipe-lite/images/Slides/slider1.jpg
		recipe-lite/images/Slides/slider2.jpg
		recipe-lite/images/Slides/slider3.jpg
        
        Slider Images Are Maxpixel Images:        
		https://www.maxpixel.net/Christmas-Stollen-Christmas-Time-Homemade-Bake-2723655
		https://www.maxpixel.net/Texture-Tomato-Cover-Table-Macro-Food-Red-2945904
		https://www.maxpixel.net/Salt-Bay-Leaf-Food-Vegetables-Mushrooms-Plate-1491723
		
		Images used are from maxpixel.net
		Both sites provide images under CC0 license
 		CC0 license: https://creativecommons.org/share-your-work/public-domain/cc0
        
7 -     recipe-lite/css/animation.css 

		Source : animate.css - https://daneden.github.io/animate.css/
		Licensed under the MIT license - http://opensource.org/licenses/MIT
        

For any help you can mail us at support[at]sktthemes.com

## Changelog
Version 1.3
i)   Updated theme as per latest theme check and added SKT Templates plugin via TGM

Version 1.1
i)   Author URI and Theme URI changed.
ii)  Removed anchor link from footer.

Version 1.0
i)   Initial version release