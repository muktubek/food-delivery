<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
<style type="text/css">

@font-face {
  font-family: "Exotc350 DmBd BT";
  src: url("/wp-content/themes/astra/fonts/Exot350D.ttf") format("truetype");
}

.menu-link{
	font-weight: bold;
}
.ast-separate-container, .ex-fdlist .exstyle-1 .exstyle-1-image{
	background-color: white;
}
#primary-menu{
  font-family: 'Exotc350 DmBd BT';
  font-size: 17px;
}

.main-header-menu > .menu-item > .menu-link{
	color: white;
}
</style>
<script type="text/javascript">
(function($) {
  $(document).ready(function(){
  	var url = window.location.protocol+'//'+window.location.hostname+'/';
  	var menuImage = url+'wp-content/uploads/2020/09/menu_img2.jpg';
  	if(window.location.href == url){
  		menuImage = url+'wp-content/uploads/2020/09/menu_img.jpg';
  		$('.main-header-bar').css('background-image','url('+menuImage+')').css('margin-left','24px').css('margin-right','38px').css('background-position-y','7px').css('background-position-x','1px');
  	}else{
  		$('.main-header-bar').css('background-image','url('+menuImage+')').css('background-repeat','no-repeat').css('background-size','contain');
  	}
 });
}(jQuery));
</script>
</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>

<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<div 
	<?php
	echo astra_attr(
		'site',
		array(
			'id'    => 'page',
			'class' => 'hfeed site',
		)
	);
	?>
>
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>

	<?php astra_header_before(); ?>

	<?php astra_header(); ?>

	<?php astra_header_after(); ?>

	<?php astra_content_before(); ?>

	<div id="content" class="site-content">

		<div class="ast-container">

		<?php astra_content_top(); ?>
